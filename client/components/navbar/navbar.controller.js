'use strict';

angular.module('tabledConfigApp')
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.menu = [
        {
            'title': 'Home',
            'link': '/'
        },
        {
            'title': 'Apps',
            'link': '#/applications'
        },
        {
            'title': 'Commands',
            'link': '#/commands'
        }];

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });