'use strict';

angular.module('tabledConfigApp')
  .filter('propertyName', function () {
    return function (input, filterVal) {
        var filteredInput = {};
        angular.forEach(input, function(value, key){
            if(key.match(new RegExp(filterVal,'i'))){
                filteredInput[key]= value;
            }
        });
        return filteredInput;
    };
  });
