'use strict';

describe('Filter: propertyName', function () {

  // load the filter's module
  beforeEach(module('tabledConfigApp'));

  // initialize a new instance of the filter before each test
  var propertyName;
  beforeEach(inject(function ($filter) {
    propertyName = $filter('propertyName');
  }));

  it('should return the input prefixed with "propertyName filter:"', function () {
    var obj = {propName: "propVal", notDisplay: "None"};
    var text = 'propnam';
    expect(propertyName(obj,text).propName).toBe('propVal');
  });

});
