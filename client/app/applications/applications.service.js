'use strict';

angular.module('tabledConfigApp')
  .factory('applications', ['$resource',
        function ($resource) {
            return $resource('api/apps/:os/:name/', {})
        }
    ]);
