'use strict';

angular.module('tabledConfigApp')
  .controller('ApplicationsCtrl', function ($scope,$http,$routeParams,applications) {
        $scope.applications = [];
        $scope.os = $routeParams.os;
        $scope.applications = applications.get({os:$scope.os});
        $scope.addApp = function() {
            if($scope.newApp === '') {
                return;
            }
          applications.save({os:$scope.os},$scope.newApp,function(){
                $scope.applications[$scope.newApp.name] = $scope.newApp.value;
                $scope.newApp = '';
            });
        };

        $scope.deleteApp = function(name) {
            applications.delete({os:$scope.os,name:name},function(){
                delete $scope.applications[name];
            });
        };
  });
