'use strict';

angular.module('tabledConfigApp')
  .config(function ($routeProvider) {
    $routeProvider
        .when('/applications/:os', {
            templateUrl: 'app/applications/applications.html',
            controller: 'ApplicationsCtrl'
        })
        .when('/applications', {
            templateUrl: 'app/applications/applications.html',
            controller: 'ApplicationsCtrl'
        });
  });
