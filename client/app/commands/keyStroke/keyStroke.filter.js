'use strict';

angular.module('tabledConfigApp')
  .filter('keyStroke', function () {
    return function (input) {
      var output = '';
      if(input.ctrl){
        output += 'Ctrl+';
      }
      if(input.cmd){
        output += '⌘+';
      }if(input.alt){
        output += 'Alt+';
      }if(input.shift){
        output += 'Shift+';
      }
      return output + input.sign.toUpperCase();
    };
  });
