'use strict';

describe('Filter: keyStroke', function () {

  // load the filter's module
  beforeEach(module('tabledConfigApp'));

  // initialize a new instance of the filter before each test
  var keyStroke;
  beforeEach(inject(function ($filter) {
    keyStroke = $filter('keyStroke');
  }));

  it('should return the input prefixed with "keyStroke filter:"', function () {
    var text = 'angularjs';
    expect(keyStroke(text)).toBe('keyStroke filter: ' + text);
  });

});
