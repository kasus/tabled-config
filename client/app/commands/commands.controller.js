'use strict';

angular.module('tabledConfigApp')
  .controller('CommandsCtrl', function ($scope,$http,commands) {
        $scope.commands = commands.get();
        $scope.addCmd = function() {
            if($scope.newCmd === '') {
                return;
            }
            commands.save($scope.newCmd,function(){
                $scope.commands[$scope.newCmd.name] = $scope.newCmd.value;
                $scope.newCmd = '';
            });
        };

        $scope.deleteCmd = function(name) {
            commands.delete({name:name},function(){
                delete $scope.commands[name];
            });
        };
    });
