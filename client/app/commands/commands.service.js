'use strict';

angular.module('tabledConfigApp')
    .factory('commands', ['$resource',
        function ($resource) {
            return $resource('api/commands/:name/', {})
        }
    ]);
