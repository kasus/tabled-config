'use strict';

angular.module('tabledConfigApp')
  .config(function ($routeProvider) {
    $routeProvider
        .when('/commands', {
        templateUrl: 'app/commands/commands.html',
        controller: 'CommandsCtrl'
      });
  });
