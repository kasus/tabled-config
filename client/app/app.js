'use strict';

angular.module('tabledConfigApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap'
])
  .config(function ($routeProvider, $locationProvider) {


    $locationProvider.html5Mode(true);
  });