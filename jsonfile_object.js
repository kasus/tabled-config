var fs = require('fs');
module.exports = function(name){
    if(!fs.existsSync(name)) {
        fs.writeFileSync(name,"{}");
    }
    try {
        var obj = JSON.parse(fs.readFileSync(name));
    }
    catch(e){
        console.log("File "+name+" is corrupted!");
        var obj = {};
    }
    obj.name = function(){
        return name;
    }
    obj.save = function(){
        try {
            fs.writeFileSync(this.name(),JSON.stringify(this,null,"\t"));
        }
        catch(e){
            return false;
        }
        return true;
    };
    return obj;
}