'use strict';

var express = require('express');
var controller = require('./app.controller');

var router = express.Router();

router.get('/:os', controller.index);
router.get('/:os/:id', controller.show);
router.post('/:os', controller.create);
router.put('/:os/:id', controller.update);
router.patch('/:os/:id', controller.update);
router.delete('/:os/:id', controller.destroy);

module.exports = router;