'use strict';

var _ = require('lodash');
var App = require('./app.model');

// Get list of apps
exports.index = function(req, res) {
  return res.json(200, App[req.params.os]);
};

// Get a single app
exports.show = function(req, res) {
  App.findById(req.params.id, function (err, app) {
    if(err) { return handleError(res, err); }
    if(!app) { return res.send(404); }
    return res.json(app);
  });
};

// Creates a new app in the DB.
exports.create = function(req, res) {
    App[req.params.os][req.body.value] = req.body.name;
    if(App.save()) {
        return res.json(201, App[req.params.os]);
    }
    return res.send(404);
};

// Updates an existing app in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  App.findById(req.params.id, function (err, app) {
    if (err) { return handleError(res, err); }
    if(!app) { return res.send(404); }
    var updated = _.merge(app, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, app);
    });
  });
};

// Deletes a app from the DB.
exports.destroy = function(req, res) {
    delete App[req.params.os][req.params.id];
    if(App.save()) {
        return res.send(204);
    }
    return res.send(404);
};

function handleError(res, err) {
  return res.send(500, err);
}